﻿using MeshProcessing.Utilities;
using UnityEngine;

public class MainController : MonoBehaviour
{
    private void Start()
    {
        Service.Create<NativeLibraryService>();
        
        Service.InitAll();
    }

    private void OnDestroy()
    {
        Service.DestroyAll();
    }
}