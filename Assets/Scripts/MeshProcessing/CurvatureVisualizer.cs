﻿using UnityEngine;
using System.Linq;
using MeshProcessing.Utilities;

namespace MeshProcessing
{
	[NativeAccessAttribute]
	[RequireComponent(typeof(MeshFilter))]
	public class CurvatureVisualizer : MonoBehaviour {

		private MeshFilter meshFilter;
		
		public Gradient Gradient = new Gradient();

		private delegate void CalculateCurvatureDelegate(ref Vector3 verts, int vertCount, ref int faces,
			int faceCount, ref float curvatures);

		[NativeMethod(LibraryName = "igl", MethodName = "calculate_curvature")]
		private static CalculateCurvatureDelegate CalculateCurvature;

		private delegate void CalculateLaplacianDeformation(ref Vector3 verts, int vertCount, ref int faces,
			int faceCount, ref float curvatures);

		[NativeMethod(LibraryName = "igl", MethodName = "laplacian_deform")]
		private static CalculateLaplacianDeformation LaplacianDeform;

		public void Start ()
		{
			meshFilter = GetComponent<MeshFilter> ();
		}

		private void Update ()
		{
			UpdateCurvatureColors ();	
		}
		
		private void UpdateCurvatureColors()
		{
			var mesh = meshFilter.mesh;

			var vertices = mesh.vertices;
			var triangles = mesh.triangles;
			var faceCount = mesh.triangles.Length / 3;
			var vertCount = mesh.vertices.Length;
			
			var curvatures = new float[vertCount];

            CalculateCurvature (ref vertices[0], vertCount, ref triangles[0], faceCount,
	            ref curvatures[0]);

            mesh.colors = curvatures.Select(curvature => Gradient.Evaluate(curvature)).ToArray();
		}
	}
}
