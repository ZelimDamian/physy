﻿using System;
using System.Reflection;

namespace MeshProcessing.Utilities
{
    [AttributeUsage(AttributeTargets.Class)]
    public class NativeAccessAttribute : Attribute { }
    
    [AttributeUsage(AttributeTargets.Field)]
    public class NativeMethodAttribute: Attribute
    {
        public string LibraryName { get; set; }
        public string MethodName { get; set; }
    }
}