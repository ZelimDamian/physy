﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.InteropServices;
using UnityEngine;

namespace MeshProcessing.Utilities
{
	public class NativeLibraryService : Service
	{
		protected override void Init()
		{
			var types = GetTypesWithAttribute<NativeAccessAttribute>(GetType().Assembly);
			foreach (var type in types)
			{
				Inject(type);
			}
		}
		
		private static void Inject(Type type)
		{
            var properties = type.GetFields(BindingFlags.Static | BindingFlags.Public | BindingFlags.NonPublic);
            foreach (var property in properties)
            {
                var nativeMethod = property.GetCustomAttribute<NativeMethodAttribute>();
                if (nativeMethod == null) continue;
                
                var handle = GetDelegate(nativeMethod.LibraryName,
                    nativeMethod.MethodName, property.FieldType);
                
                property.SetValue(null, handle);
            }
		}

#if UNITY_EDITOR_OSX
		private const string LibSuffix = ".bundle";
#elif UNITY_EDITOR_LINUX
        private const string LibSuffix = ".so";
#elif UNITY_EDITOR_WIN
        private const string LibSuffix = ".dll";
#endif

		private static readonly Dictionary<string, IntPtr> Libraries = new Dictionary<string, IntPtr>();

		private static Delegate GetDelegate(string libraryName, string functionName, Type delegateType)
		{
			var libraryHandle = GetHandle(libraryName);
			return GetDelegate(libraryHandle, functionName, delegateType);
		}

		protected override void Destroy()
		{
			foreach (var pair in Libraries)
			{
				CloseLibrary(pair.Value);
			}
		}

		private static IEnumerable<Type> GetTypesWithAttribute<T>(Assembly assembly, bool inherit = false)
			where T : Attribute
		{
			foreach (var type in assembly.GetTypes())
			{
				var attribute = type.GetCustomAttribute<T>(inherit);
				if (attribute != null) yield return type;
			}
		}


		private static IntPtr GetHandle(string libraryName)
		{
			if (!Libraries.ContainsKey(libraryName))
			{
				Libraries[libraryName] = OpenLibrary($"{Application.dataPath}/Native/{libraryName}{LibSuffix}");
			}
			return Libraries[libraryName];
		}


#if UNITY_EDITOR_OSX || UNITY_EDITOR_LINUX

		[DllImport("__Internal")]
		private static extern IntPtr dlopen(
			string path,
			int flag);

		[DllImport("__Internal")]
		private static extern IntPtr dlsym(
			IntPtr handle,
			string symbolName);

		[DllImport("__Internal")]
		private static extern int dlclose(
			IntPtr handle);

		private static IntPtr OpenLibrary(string path)
		{
			var handle = dlopen(path, 0);
			if (handle == IntPtr.Zero)
			{
				throw new Exception("Couldn't open native library: " + path);
			}
			return handle;
		}

		private static void CloseLibrary(IntPtr libraryHandle)
		{
			dlclose(libraryHandle);
		}

		private static T GetDelegate<T>(IntPtr libraryHandle, string functionName) where T : class
		{
			return GetDelegate(libraryHandle, functionName, typeof(T)) as T;
		}

		private static Delegate GetDelegate(IntPtr libraryHandle, string functionName, Type delegateType)
		{
			var symbol = dlsym(libraryHandle, functionName);
			if (symbol == IntPtr.Zero)
			{
				throw new Exception("Couldn't get function: " + functionName);
			}

			return Marshal.GetDelegateForFunctionPointer(symbol, delegateType);
		}


#elif UNITY_EDITOR_WIN

        [DllImport("kernel32")]
        private static extern IntPtr LoadLibrary( string path);
     
        [DllImport("kernel32")]
        private static extern IntPtr GetProcAddress( IntPtr libraryHandle, string symbolName);
     
        [DllImport("kernel32")]
        private static extern bool FreeLibrary( IntPtr libraryHandle); 
		
		private static IntPtr OpenLibrary(string path)
        {
            var handle = LoadLibrary(path);
            if (handle == IntPtr.Zero)
            {
                throw new Exception("Couldn't open native library: " + path);
            }
            return handle;
        }

		private static void CloseLibrary(IntPtr libraryHandle)
        {
            FreeLibrary(libraryHandle);
        }
     
		private static Delegate GetDelegate(IntPtr libraryHandle, string functionName, Type delegateType)
		{
            var symbol = GetProcAddress(libraryHandle, functionName);
			if (symbol == IntPtr.Zero)
			{
				throw new Exception("Couldn't get function: " + functionName);
			}

			return Marshal.GetDelegateForFunctionPointer(symbol, delegateType);
		}
     
#endif
	}
}
