﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;



namespace MeshProcessing.Utilities
{
    public abstract class Service 
    {
        private static readonly Dictionary<Type, Service> Services = new Dictionary<Type, Service>();

        public static void Create<TService>() where TService: Service, new()
        {
            Services.Add(typeof(TService), new TService());
        }
        
        public static T Get<T>() where T : Service
        {
            return Services.ContainsKey(typeof(T)) ? Services[typeof(T)] as T : null;
        }

        protected virtual void Init() { }
        protected virtual void Destroy() { }

        public static void InitAll()
        {
            foreach (var service in Services)
            {
                service.Value.Init();
            }
        }

        public static void DestroyAll()
        {
            foreach (var service in Services)
            {
                service.Value.Destroy();
            }
        }
    }
}
