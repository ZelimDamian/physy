﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NBodyCompute : MonoBehaviour {

	const int NUM_THREADS = 16;

	public int Count = 10;

	public float Damping = 0.1f;

	public ComputeShader computeShader;
	public GameObject particlePrefab;

	ComputeBuffer computeBuffer;

	Node[] nodes;
	GameObject[] nodePrefabs;

	public Attractor Attractor;

	// Use this for initialization
	void Start ()
	{
		nodes = new Node[Count];
		computeBuffer = new ComputeBuffer (nodes.Length, Node.GetSize(), ComputeBufferType.Default);

		ResetNodes ();
		InstantiateParticles ();
		computeShader.SetFloat ("N", nodes.Length);

		var kernelIndex = computeShader.FindKernel ("NBody");
		computeShader.SetBuffer(kernelIndex, "nodes", computeBuffer);
	}

	void InstantiateParticles() {
		nodePrefabs = new GameObject[nodes.Length];
		for (int i = 0; i < nodes.Length; i++) {
			nodePrefabs[i] = Instantiate (particlePrefab);
		}

        DownloadPositions();
		UploadNodes();
    }

    void ResetNodes()
	{
		for (int i = 0; i < nodes.Length; i++)
		{
			nodes [i].pos = RandomVec4 ();
			nodes [i].vel = RandomVec4 () / 10.0f;
			nodes [i].col = RandomVec4 ();
			nodes [i].mass = 1.0f;
		}

		computeBuffer.SetData (nodes);
    }

    void UploadNodes()
    {
        for (int i = 0; i < nodes.Length; i++)
        {
            nodes[i].pos = nodePrefabs[i].transform.position;
            nodes[i].mass = nodePrefabs[i].GetComponent<Rigidbody>().mass;
        }

        computeBuffer.SetData(nodes);
    }

    void DownloadPositions()
    {
        var nodes = new Node[this.nodes.Length];
        computeBuffer.GetData(nodes);

        for (int i = 0; i < nodes.Length; i++)
        {
            nodePrefabs[i].transform.position = nodes[i].pos;
        }
    }

    void DownloadForces()
    {
        var nodes = new Node[this.nodes.Length];
        computeBuffer.GetData(nodes);

        for (int i = 0; i < nodes.Length; i++)
        {
            nodePrefabs[i].GetComponent<Rigidbody>().AddForce(nodes[i].acc, ForceMode.Acceleration);
        }
    }

    Vector4 RandomVec4(float min = -5.0f, float max = 5.0f)
	{
		return new Vector4 (Random.Range (min, max), Random.Range (min, max), Random.Range (min, max), Random.Range (min, max));
	}

	RenderTexture CreateRenderTexture()
	{
		var renderTexture = new RenderTexture (512, 512, 24);
		renderTexture.enableRandomWrite = true;
		renderTexture.Create ();
		return renderTexture;
	}
	
	// Update is called once per frame
	void FixedUpdate ()
	{

        var kernelIndex = computeShader.FindKernel ("NBody");
		computeShader.Dispatch (kernelIndex, this.nodes.Length / NUM_THREADS, 1, 1);
		computeShader.SetFloat ("DeltaTime", Time.fixedDeltaTime);
		computeShader.SetFloat ("M", Attractor.Mass);
		computeShader.SetFloat ("D", Damping);

		computeShader.SetVector ("Attractor", Attractor.transform.position);
        
        DownloadForces();
        //UploadNodes();
    }

    void OnDestroy()
	{
		computeBuffer.Release ();
	}
}
