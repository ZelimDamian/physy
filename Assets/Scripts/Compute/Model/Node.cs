﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct Node {
	public Vector4 pos;
	public Vector4 vel;
	public Vector4 acc;
    public Vector3 col;
	public float mass;

	public override string ToString()
	{
		return pos.ToString() + "/" + vel.ToString();
	}

	public static int GetSize()
	{
		//return System.Runtime.InteropServices.Marshal.SizeOf(typeof(Node));
		return 4 * 4 + 4 * 4 + 3 * 4 + 4;
	}
};